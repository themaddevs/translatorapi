package de.spaceparrots.translator.api;

import de.spaceparrots.translator.core.DictionaryFile;
import de.spaceparrots.translator.core.Language;
import de.spaceparrots.translator.core.OriginalText;
import de.spaceparrots.translator.core.Translation;

import java.util.Collection;

/**
 * <h1>IDictionary</h1>
 * A IDictionary contains all translations and registered languages.
 * Not every OriginalText contains a translation for every language.<br><br>
 * The Language "English" -- "en" is implemented for the default global language.
 * Commonly English is used withing Java software, which lead to this decision.
 *
 * @author Drayke
 * @version 1.0
 * @since 04.01.2018
 */
public interface IDictionary
{

    /**
     * This constant is used to replace arguments within a translation.
     * Example: tr("## is awesome.","Drayke");
     */
    String _ARG_ = "##";

    /**
     * The constant DEFAULT_LANGUAGE = new Language( "en", "English" )
     */
    Language DEFAULT_LANGUAGE = new Language( "en", "English" );

    /**
     * Gets default language.
     *
     * @return the default language
     */
    default Language getDefaultLanguage()
    {
        return DEFAULT_LANGUAGE;
    }

    /**
     * Registers a dictionary file.
     *
     * @param dictionaryFile the dictionary file
     */
    void registerDictionaryFile( DictionaryFile dictionaryFile );

    /**
     * Registers a dictionary file and calls {@link IRegisterStatusCallback#onDuplicates(int, int, int)}
     * to return a feedback about the registering process.
     *
     * @param dictionaryFile the dictionary file
     * @param callback       the callback
     */
    void registerDictionaryFile( DictionaryFile dictionaryFile, IRegisterStatusCallback callback );

    /**
     * Checks if the dictionary contains a language.
     *
     * @param languageKey the language key
     *
     * @return true if language exists, else false
     */
    boolean hasLanguage( final String languageKey );

    /**
     * Gets a {@link Language} by a language key.
     *
     * @param languageKey the language key
     *
     * @return the language
     */
    Language getLanguage( String languageKey );

    /**
     * Adds a {@link Language} to the dictionary.
     *
     * @param languageName the language name
     * @param languageKey  the language key
     *
     * @return boolean result
     */
    boolean addLanguage( String languageName, String languageKey );

    /**
     * Checks if the Directory has any translation.
     *
     * @param originalText the original text
     *
     * @return true if there is an OriginalText object present for originalText
     */
    boolean hasAnyTranslation( final String originalText );

    /**
     * Checks if the Directory has any translation for a given language.
     *
     * @param originalText the original text
     * @param languageKey  the language key
     *
     * @return true if a translation exists
     */
    boolean hasAnyTranslation( final String originalText, String languageKey );

    /**
     * Gets all languages.
     *
     * @return the language list
     */
    Collection<Language> getLanguages();

    /**
     * Gets all {@link OriginalText} entries.
     *
     * @return the OriginalText entry list
     */
    Collection<? extends IOriginalText> getEntries();

    /**
     * Gets a translation by a language key and the original text.
     *
     * @param languageKey  the language key
     * @param originalText the original text
     *
     * @return the translation
     */
    Translation getTranslation( final String languageKey, final String originalText );
}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/