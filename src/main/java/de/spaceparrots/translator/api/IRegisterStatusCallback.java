package de.spaceparrots.translator.api;

import de.spaceparrots.translator.core.Dictionary;
import de.spaceparrots.translator.core.DictionaryFile;

/**
 * <h1>IRegisterStatusCallback</h1>
 * IRegisterStatusCallback can be used to get information
 * about the register process at the Dictionary.
 *
 * @author Drayke
 * @version 1.0
 * @see Dictionary#registerDictionaryFile(DictionaryFile)
 * @since 18.07.2017
 */
@FunctionalInterface
public interface IRegisterStatusCallback
{

    /**
     * Gets the duplicates from the register process at the Dictionary.
     *
     * @param languageDuplicates    the amount of language duplicates
     * @param translationDuplicated the amount of translation duplicated
     * @param mergedTranslations    the amount of merged or overwritten translations
     *
     * @see Dictionary#registerDictionaryFile(DictionaryFile)
     */
    void onDuplicates( int languageDuplicates, int translationDuplicated, int mergedTranslations );

    /**
     * Deprecated!
     * Calls onDuplicates( languageDuplicates,translationDuplicated,0 );
     *
     * @param languageDuplicates    the amount of language duplicates
     * @param translationDuplicated the amount of translation duplicated
     *
     * @see Dictionary#registerDictionaryFile(DictionaryFile)
     */
    @Deprecated
    default void onDuplicates( int languageDuplicates, int translationDuplicated )
    {
        onDuplicates( languageDuplicates, translationDuplicated, 0 );
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/