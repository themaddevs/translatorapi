package de.spaceparrots.translator.api;

import de.spaceparrots.translator.core.Language;

/**
 * <h1>ILanguageProvider</h1>
 * A ILanguageProvider contains a language and can be used
 * within the Translator.{@link Translator#tr(ILanguageProvider, String, Object...)}
 *
 * @author Drayke
 * @version 1.0
 * @since 06.01.2018
 */
public interface ILanguageProvider
{

    /**
     * Gets a {@link Language}
     *
     * @return the Language
     */
    Language getLanguage();
}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/