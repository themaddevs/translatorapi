package de.spaceparrots.translator.api;

/**
 * <h1>IArgumentConverter</h1>
 * A interface for converting objects to
 * useful argument replacements
 * within a translation.
 *
 *
 * @author Drayke
 * @version 1.0
 * @since 09.06.2017
 */
public interface IArgumentConverter<ParamClass>
{

    /**
     * Converts an object into a string
     *
     * @param object the object to convert
     *
     * @return the converted String
     */
    String convert( ParamClass object );

    /**
     * Gets the used generic class.
     *
     * @return the generic class
     */
    Class<ParamClass> getType();
}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/