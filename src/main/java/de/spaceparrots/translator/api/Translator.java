package de.spaceparrots.translator.api;

import de.spaceparrots.translator.core.Dictionary;
import de.spaceparrots.translator.core.Language;
import de.spaceparrots.translator.util.exception.ArgumentConverterException;

/**
 * <h1>The Translation API</h1>
 *
 * @author Drayke
 * @version 1.0
 * @since 09.06.2017
 */
public final class Translator
{

    private static final ArgumentConverter argumentConverter = new ArgumentConverter();

    private static IDictionary dictionary = new Dictionary();

    /**
     * Gets the dictionary.
     *
     * @return the dictionary
     */
    public static IDictionary getDictionary()
    {
        return Translator.dictionary;
    }

    /**
     * Sets the dictionary as a shared instance for the translator.
     *
     * @param dictionary the dictionary
     */
    public static void setDictionary( IDictionary dictionary )
    {
        if ( Translator.dictionary == null )
            Translator.dictionary = dictionary;
    }

    /**
     * Resets the dictionary instance!
     * <br>
     * Be carefull!
     */
    public static void resetDictionary()
    {
        Translator.dictionary = null;
    }

    /**
     * Gets the {@link Language} for a given language key.
     * Commonly the language key is a shortcut for the
     * language name. By default the Language "English" is
     * registered with the language key "en".
     *
     * @param languageKey the language key
     *
     * @return a language
     */
    public static Language getLanguage( String languageKey )
    {
        return getDictionary().getLanguage( languageKey );
    }

    /**
     * Gets the configured global {@link Language}.
     * By default the Language is "English" with key: "en".
     *
     * @return the global language
     */
    public static Language getDefaultLanguage()
    {
        return getDictionary().getDefaultLanguage();
    }

    /**
     * Gets the converted object string.
     * The most suitable converter will be selected for this. If no
     * converter is available for the Type object.toString() is returned.
     *
     * @param object the object
     *
     * @return the converted object string
     *
     * @throws ArgumentConverterException
     */
    public static <T> String convert( T object )
    {
        return argumentConverter.convert( object );
    }

    /**
     * Translates the original text into the given language.
     * {@link Translator#getDefaultLanguage()} is used by default.
     * If you want to parse arguments here, you need to use
     * <pre>null</pre> as the first parameter.<br>
     * tr(null,"Text: ##",arg,..);
     *
     * @param originalText the original text
     *
     * @return the translated string
     *
     * @see Translator#tr(String, String, Object...) for full description
     */
    public static String tr( String originalText )
    {
        return tr( getDefaultLanguage(), originalText );
    }

    /**
     * Translates the original text into the given language.
     * If the language parameter is null it will be replaced
     * by the global language.
     *
     * @param language     the language
     * @param originalText the original text
     *
     * @return the translated string
     *
     * @see Translator#tr(String, String, Object...) for full description
     */
    public static String tr( Language language, String originalText )
    {
        if ( language == null )
            language = getDefaultLanguage();
        return tr( language.getLanguageKey(), originalText );
    }

    /**
     * Translates the original text into the given language.
     * If the languageKey parameter is null it will be replaced
     * by the global language.
     *
     * @param languageKey  the language key
     * @param originalText the original text
     *
     * @return the translated string
     *
     * @see Translator#tr(String, String, Object...) for full description
     */
    public static String tr( String languageKey, String originalText )
    {
        return tr( languageKey, originalText, new Object[0] );
    }

    /**
     * Translates the original text into the given language.
     * {@link Translator#getDefaultLanguage()} is used by default.
     *
     * @param originalText the original text
     * @param args         the args
     *
     * @return the translated string
     *
     * @see Translator#tr(String, String, Object...) for full description
     */
    public static String tr( String originalText, Object... args )
    {
        return tr( getDefaultLanguage(), originalText, args );
    }

    /**
     * Translates the original text into the given language.
     * If the language parameter is null it will be replaced
     * by the global language.
     *
     * @param language     the language
     * @param originalText the original text
     * @param args         the args
     *
     * @return the translated string
     *
     * @see Translator#tr(String, String, Object...) for full description
     */
    public static String tr( Language language, String originalText, Object... args )
    {
        if ( language == null )
            language = getDefaultLanguage();
        return tr( language.getLanguageKey(), originalText, args );
    }

    /**
     * Translates the original text into the given language.
     * The Language is received by the ILanguageProvider
     *
     * @param languageProvider the language provider
     * @param originalText     the original text
     * @param args             the args
     *
     * @return the translated string
     *
     * @see Translator#tr(String, String, Object...) for full description
     */
    public static String tr( ILanguageProvider languageProvider, String originalText, Object... args )
    {
        return tr( languageProvider.getLanguage(), originalText, args );
    }

    /**
     * Translates the original text into the given language.
     * If the language parameter is null it will be replaced
     * by the global language.
     *
     * <ul>
     * <li>Without a language key, the default Language will be used to translate
     * the text.</li>
     * <li>Placeholders (@See IDictionary#_ARG_) in the original text can be used
     * and replaced by arguments.</li>
     * <li>Arguments are replaced with the (@code toString()) method of the Object by default.</li>
     * <li>For better results: add some (@code IArgumentConverter) for the Converter or overwrite the (@code toString()) method</li>
     * <li>Arguments are replaced with the <pre>toString()</pre> method of the Object by default.</li>
     * <li>For better results: add some (@See IArgument) for the Converter or overwrite the <pre>toString()</pre> method</li>
     * <li>If no translation can be found for the given parameter, the original text will be returned</li>
     * </ul>
     *
     * @param languageKey  the language key
     * @param originalText the original text
     * @param args         the arguments
     *
     * @return the translated string
     *
     * @see IDictionary#_ARG_
     */
    public static String tr( String languageKey, String originalText, Object... args )
    {
        if ( languageKey == null )
            languageKey = getDefaultLanguage().getLanguageKey();

        //Gets the translation. If no translation is available, the originalText will be returned
        String translation = getDictionary() == null ? originalText : getDictionary().getTranslation( languageKey, originalText ).getTranslated();

        //Replace arguments
        String replacement = "";
        for ( Object obj : args )
        {
            replacement = argumentConverter.convert( obj );
            translation = translation.replaceFirst( IDictionary._ARG_, replacement );
        }

        return translation;
    }

    /**
     * Registers a {@link IArgumentConverter}
     *
     * @param clazz     the class to convert
     * @param converter the converter
     * @param <T>       the type
     */
    public static <T> void registerConverter( Class<T> clazz, IArgumentConverter<T> converter )
    {
        argumentConverter.registerConverter( clazz, converter );
    }

    /**
     * Checks if a {@link IArgumentConverter} for a class/type is present
     *
     * @param clazz the class to compare
     *
     * @return true if a IArgumentConverter is present.
     */
    public static boolean hasConverter( Class<?> clazz )
    {
        return argumentConverter.hasConverter( clazz );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/