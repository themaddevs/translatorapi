package de.spaceparrots.translator.api;

import de.spaceparrots.translator.util.exception.ArgumentConverterException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * <h1>ArgumentConverter</h1>
 * The ArgumentConverter provides support to translate argument objects.
 * This class is only visible to the package.
 *
 * @author Drayke
 * @version 1.0
 * @since 19.07.2017
 */
class ArgumentConverter
{

    private static HashMap<Class, IArgumentConverter> converters = new HashMap<>();


    /**
     * Registers a {@link IArgumentConverter}
     *
     * @param clazz     the class to convert
     * @param converter the converter
     * @param <T>       the type
     */
    public <T> void registerConverter( Class<T> clazz, IArgumentConverter<T> converter )
    {
        if ( clazz == null || converter == null ) return;
        converters.put( clazz, converter );
    }

    /**
     * Gets the converted object.
     *
     * @param object the object
     *
     * @return the converted object to string
     *
     * @throws ArgumentConverterException
     */
    public <T> String convert( T object )
    {
        if ( object == null ) return "";

        List<Class> collect = new ArrayList<>();
        collect.addAll( converters.keySet().stream().filter( aClass -> aClass.isAssignableFrom( object.getClass() ) ).collect( toList() ) );

        if ( collect.isEmpty() )
            return object.toString();

        collect.sort( ( class1, class2 ) -> {
            //If equals
            if ( class1.equals( class2 ) )
                return 0;

            if ( class1.isAssignableFrom( class2 ) )
                return 1;

            if ( class2.isAssignableFrom( class1 ) )
                return -1;

            return -1;
        } );

        IArgumentConverter<T> converter = converters.get( collect.get( 0 ) );

        if ( converter == null ) throw new ArgumentConverterException( object.getClass(), "No valid converter found!" );

        String argument = converter.convert( object );
        if ( argument == null )
            throw new ArgumentConverterException( object.getClass(), "The converter returned an invalid result!" );

        return argument;
    }


    /**
     * Checks if a IArgumentConverter for a class/type is present
     *
     * @param aClass to compare
     *
     * @return true if a IArgumentConverter is present.
     */
    public boolean hasConverter( Class<?> aClass )
    {
        return converters.containsKey( aClass );
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/