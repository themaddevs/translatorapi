package de.spaceparrots.translator.api;

import de.spaceparrots.translator.core.OriginalText;
import de.spaceparrots.translator.core.Translation;

import java.util.List;

/**
 * <h1>IOriginalText</h1>
 * The IOriginalText interface.
 *
 * @author Drayke
 * @version 1.0
 * @since 05.01.2018
 */
public interface IOriginalText
{

    /**
     * Gets the text.
     *
     * @return the original text
     */
    String getText();

    /**
     * Gets all translations for this text.
     *
     * @return the Translation list
     */
    List<Translation> getTranslations();

    /**
     * Gets the original text as a Translation object.
     *
     * @return the original Translation
     */
    Translation getOriginal();

    /**
     * Merges a {@link OriginalText} with the calling instance.
     *
     * @param other the other OriginalText
     *
     * @return the int
     */
    int merge( IOriginalText other );

    /**
     * Adds a translation.
     * A translation will be added when there is no one present for
     * the given language, or when the present translation is replaceable.
     *
     * @param languageKey the language key
     * @param translation the translation
     * @param replaceable the replaceable
     */
    void addTranslation( String languageKey, String translation, boolean replaceable );

    /**
     * Gets a translation by a language key.
     *
     * @param languageKey the language key
     *
     * @return the translation
     */
    Translation getTranslation( String languageKey );

    /**
     * Checks if a translation is available.
     *
     * @param languageKey the language key
     *
     * @return true if a translation is available
     */
    boolean hasTranslation( String languageKey );

    /**
     * Compares the original texts.
     *
     * @param text the text
     *
     * @return true if original text is equal
     */
    boolean equals( String text );

}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/