package de.spaceparrots.translator.core;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * <h1>Translation</h1>
 * A Translation for a one language.
 * NOTE: The original text is not stored
 * within this class.
 *
 * @author Drayke
 * @version 1.0
 * @since 09.06.2017.
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
public final class Translation implements Comparable<Translation>
{

    /**
     * The language identifier
     */
    private final String language;

    /**
     * The translated text corresponding to the
     * translation tag
     */
    private String translated;

    /**
     * A boolean tag if this translation can
     * be replaced when another dictionary file
     * is registered. Default = false.
     */
    private final boolean replaceable;

    public Translation( String language, String translated )
    {
        this( language, translated, false );
    }

    /**
     * Is there a translated String available.
     * If the language is is empty, this method also returns
     * false!
     *
     * @return true if translation is available, else false
     */
    public boolean isTranslated()
    {
        return !language.isEmpty() && !translated.isEmpty();
    }

    /**
     * Sets the translation. This method requires this object
     * to be replaceable. This just can be influenced by the registered xml file.
     *
     * @param translated the translated string
     *
     * @return true is replaceable, else false
     */
    public boolean setTranslation( String translated )
    {
        //Not protected and not the same translation
        if ( isReplaceable() && !translated.equals( this.translated ) )
        {
            this.translated = translated;
            return true;
        }
        return false;
    }

    @Override
    public int compareTo( Translation other )
    {
        return ( other.getLanguage().equals( this.language ) &&
                other.getTranslated().equals( this.translated ) ) ? 0 : 1;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/