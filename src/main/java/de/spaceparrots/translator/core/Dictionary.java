package de.spaceparrots.translator.core;

import de.spaceparrots.translator.api.IDictionary;
import de.spaceparrots.translator.api.IOriginalText;
import de.spaceparrots.translator.api.IRegisterStatusCallback;
import de.spaceparrots.translator.util.Pair;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * <h1>Dictionary</h1>
 * The {@link IDictionary} implementation.
 * A Dictionary contains all translations and registered languages.
 * Not every OriginalText contains a translation for every language.<br><br>
 * The Language "English" -- "en" is implemented for the default global language.
 * Commonly English is used withing Java software, which leaded to this decision.
 *
 * @author Drayke
 * @version 1.0
 * @since 14.07.2017
 */
public final class Dictionary implements IDictionary
{

    private HashSet<OriginalText> entries = new HashSet<>();

    private HashSet<Language> languages = new HashSet<>();

    public Dictionary()
    {

    }

    @Override
    public HashSet<Language> getLanguages()
    {
        return this.languages;
    }

    @Override
    public HashSet<OriginalText> getEntries()
    {
        return this.entries;
    }

    /**
     * Registers a dictionary file. All translations and
     * languages within this file will be checked and added to the
     * dictionary. Duplicated will be ignored.
     *
     * @param dictionaryFile the dictionary file
     */
    @Override
    public void registerDictionaryFile( DictionaryFile dictionaryFile )
    {
      registerDictionaryFile( dictionaryFile, null );
    }

    /**
     * Registers a dictionary file. All translations and
     * languages within this file will be checked and added to the
     * dictionary. Duplicated will be ignored but the amount of
     * duplicates can be received with the (@code IRegisterStatusCallback)
     * callback interface.
     *
     * @param dictionaryFile the dictionary file
     * @param callback       a function beeing executed when duplicates appear
     *
     * @see IRegisterStatusCallback#onDuplicates IRegisterStatusCallback#onDuplicates
     */
    @Override
    public void registerDictionaryFile( DictionaryFile dictionaryFile, IRegisterStatusCallback callback )
    {
        Pair<List<Language>, List<OriginalText>> storedContent = dictionaryFile.getStoredContent();

        int langDuplicates = 0;
        int translationDuplicates = 0;
        int merged = 0;

        //Extract languages
        for ( Language language : storedContent.getFirst() )
        {
            if ( !languages.add( language ) )
                langDuplicates++;
        }

        //Extract translations
        for ( OriginalText originalText : storedContent.getSecond() )
        {
            if ( !entries.add( originalText ) )
            {
                //Get the original text that is already present
                Optional<OriginalText> first = entries.stream().filter( oText -> oText.equals( originalText.getText() ) ).findFirst();
                if ( first.isPresent() )
                {
                    IOriginalText text = first.get();
                    //merge current with new original text
                    merged += text.merge( originalText );
                }
            }
            translationDuplicates++;
        }

        if ( callback!=null && ( langDuplicates != 0 || translationDuplicates != 0 ) )
            callback.onDuplicates( langDuplicates, translationDuplicates, merged );
    }

    /**
     * Registers a language. Returns false
     * if language already exists.
     *
     * @param languageName the language name
     * @param languageKey  the language key
     *
     * @return the boolean if successful, else false
     */
    @Override
    public boolean addLanguage( String languageName, String languageKey )
    {
        if ( hasLanguage( languageKey ) )
            return false;
        languages.add( new Language( languageKey, languageName ) );
        return true;
    }

    /**
     * Checks if a language with a given language key
     * is registered in this Dictionary.
     *
     * @param languageKey the language key
     *
     * @return true if a language is available, else false
     */
    @Override
    public boolean hasLanguage( final String languageKey )
    {
        Optional<Language> lang = languages.stream().filter( language -> language.getLanguageKey().equals( languageKey ) ).findFirst();
        return lang.isPresent();
    }


    /**
     * Gets a language with a given language key. If no language
     * is registered in this Dictionary null will be returned.
     *
     * @param languageKey the language key
     *
     * @return a language or null
     */
    @Override
    public Language getLanguage( final String languageKey )
    {
        Optional<Language> lang = languages.stream().filter( language -> language.getLanguageKey().equals( languageKey ) ).findFirst();
        if(lang.isPresent())
            return lang.get();
        return null;
    }

    /**
     * Checks if the Dictionary has any translation for
     * a given original text.
     *
     * @param originalText the original text
     *
     * @return true if a translation is available, else false
     */
    @Override
    public boolean hasAnyTranslation( final String originalText )
    {
        return entries.stream().filter( originalTextObj -> originalTextObj.getText().equals( originalText ) ).count() > 0;
    }

    /**
     * Check if the Dictionary has a specific translation for
     * a given original text. The language of the translation is given
     * by the languageKey parameter.
     *
     * @param originalText  the original text
     * @param languageKey   the language key
     *
     * @return true if a translation is available, else false
     */
    @Override
    public boolean hasAnyTranslation( final String originalText, String languageKey )
    {
        return entries.stream().filter( originalTextObj -> originalTextObj.getText().equals( originalText ) && originalTextObj.hasTranslation( languageKey ) ).count() > 0;
    }

    /**
     * Gets a translation for a original text for specified language.
     * If no Translation is found, the original will be returns as
     * translation. If you want to check if a translation is a real
     * translated one, use (@code Translation.isTranslated() ) for validation.
     *
     * @param languageKey  the language key
     * @param originalText the original text
     *
     * @return the translation or original text if no translation has been found.
     *
     * @see Translation
     */
    @Override
    public Translation getTranslation( final String languageKey, final String originalText )
    {
        Optional<OriginalText> result = entries.stream().filter( text -> text.getText().equals( originalText ) ).findFirst();
        if ( result.isPresent() )
        {
            Translation translation = result.get().getTranslation( languageKey );
            if ( translation != null )
                return translation;
        }
        return new OriginalText( originalText ).getOriginal();
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/