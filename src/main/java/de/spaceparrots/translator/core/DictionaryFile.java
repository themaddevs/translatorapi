package de.spaceparrots.translator.core;


import de.spaceparrots.translator.api.IOriginalText;
import de.spaceparrots.translator.util.AXmlStorageFile;
import de.spaceparrots.translator.util.Pair;
import de.spaceparrots.translator.util.exception.TranslatorFileException;
import de.spaceparrots.translator.util.exception.TranslatorFileParsingException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>DictionaryFile</h1>
 * A DictionaryFile contains required data for a
 * Dictionary. The Class parses Languages and Translations( within an OriginalText)
 * from and to an XML-File.
 *
 * @author Drayke
 * @version 1.0
 * @since 09.06.2017
 */
public final class DictionaryFile extends AXmlStorageFile
{

    private static final String TRANSLATION_TAG_NAME = "translation";
    private static final String TRANSLATION_BEGIN = "translations";
    private static final String TAG_TEXT_NAME = "text";
    private static final String TRANSLATION_ATTR_NAME = "original";
    private static final String ATTR_LANG_NAME = "lang";

    private static final String LANGUAGE_BEGIN = "languages";
    private static final String LANGUAGE_TAG_NAME = "language";
    private static final String LANGUAGE_ATTR_NAME = "key";
    public static final String PROTECTED_TAG = "protected";


    /**
     * Instantiates a new Dictionary file.
     *
     * @param inputStream the input stream
     */
    public DictionaryFile( InputStream inputStream )
    {
        super( inputStream );
    }

    /**
     * Instantiates a new Dictionary file.
     *
     * @param outputStream the output stream
     */
    public DictionaryFile( OutputStream outputStream )
    {
        super( outputStream );
    }

    public Pair<List<Language>, List<OriginalText>> getStoredContent()
    {
        ArrayList<OriginalText> translations = new ArrayList<>();
        ArrayList<Language> languages = new ArrayList<>();

        try
        {
            if ( getDataFileInStream() == null )
                throw new TranslatorFileException( TranslatorFileException.Cause.STREAM_NULL, "Can't get stored content without an InputStream!" );

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse( getDataFileInStream() );

            Node root = document.getFirstChild(); // ROOT = <dictionary>
            NodeList children = root.getChildNodes();
            for ( int i = 0; i < children.getLength(); i++ )
            {
                Node child = children.item( i );

                //Find elements
                if ( child.getNodeType() != Node.ELEMENT_NODE ) continue;
                Element element = (Element) child;

                //Check for correct elements
                if ( element.getTagName().equals( LANGUAGE_BEGIN ) )
                {
                    parseLanguage( languages, child );
                }
                if ( element.getTagName().equals( TRANSLATION_BEGIN ) )
                {
                    parseTranslation( translations, child );
                }
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }

        return new Pair<>( languages, translations );
    }

    private void parseLanguage( ArrayList<Language> languages, Node child )
    {
        NodeList languageNodes = child.getChildNodes();     //List with <language>
        for ( int i = 0; i < languageNodes.getLength(); i++ )
        {
            Node languageNode = languageNodes.item( i );
            if ( languageNode.getNodeType() != Node.ELEMENT_NODE ) continue;
            Element languageElement = (Element) languageNode;               // <language key="test">

            if ( !languageElement.getTagName().equalsIgnoreCase( LANGUAGE_TAG_NAME ) )
                throw new TranslatorFileParsingException( "Unsupported tag: " + languageElement.getTagName() + ", <language key=\"en\"> is required!" );

            String languageKey = languageElement.getAttribute( LANGUAGE_ATTR_NAME );  // "en"
            if ( languageKey == null || languageKey.isEmpty() )
                throw new TranslatorFileParsingException( "No language key found within a \"language\" tag! -->" + languageElement.toString() );
            String languageName = languageElement.getTextContent();
            if ( languageName == null || languageName.isEmpty() )
                throw new TranslatorFileParsingException( "No language name found for language key: " + languageKey );

            languages.add( new Language( languageKey, languageName ) );

        }
    }

    private void parseTranslation( ArrayList<OriginalText> translations, Node child )
    {
        NodeList translationNodes = child.getChildNodes();     //List with <translation>
        for ( int i = 0; i < translationNodes.getLength(); i++ )
        {
            Node translationNode = translationNodes.item( i );
            if ( translationNode.getNodeType() != Node.ELEMENT_NODE ) continue;
            Element translationElement = (Element) translationNode;               // <translation orginal="test">

            if ( !translationElement.getTagName().equalsIgnoreCase( TRANSLATION_TAG_NAME ) )
                throw new TranslatorFileParsingException( "Unsupported tag: " + translationElement.getTagName() + ", <translation orginal=\"text\"> is required!" );

            String originalText = translationElement.getAttribute( TRANSLATION_ATTR_NAME );  // "test"

            if ( originalText == null || originalText.isEmpty() )
                throw new TranslatorFileParsingException( "The \"orginal\" attribute for the translation tag is missing! -->" + translationElement.toString() );

            OriginalText original = new OriginalText( originalText );

            //Get translations for the orginal text
            NodeList languageNodes = translationNode.getChildNodes();
            for ( int k = 0; k < languageNodes.getLength(); k++ )
            {
                Node languageNode = languageNodes.item( k );
                if ( languageNode.getNodeType() != Node.ELEMENT_NODE ) continue;
                Element languageElement = (Element) languageNode;
                String languageKey = languageElement.getAttribute( ATTR_LANG_NAME );

                if ( languageKey == null || languageKey.isEmpty() )
                    throw new TranslatorFileParsingException( "The \"lang\" attribute for the text tag is missing! -->" + languageElement.toString() );

                //Check for protected tag in translation
                boolean protect = languageElement.hasAttribute( PROTECTED_TAG );

                String translation = languageNode.getTextContent();

                if ( translation.isEmpty() )
                    throw new TranslatorFileParsingException( "The translation for: " + original + " language: " + languageKey + " is missing!" );

                original.addTranslation( languageKey, translation, !protect );
            }

            //Add all translation for original
            translations.add( original );
        }
    }

    public void parseContent( Pair<List<Language>, List<? extends IOriginalText>> content )
    {

        try
        {
            if ( getDataFileOutStream() == null )
                throw new TranslatorFileException( TranslatorFileException.Cause.STREAM_NULL, "Can't store content without an OutputStream!" );

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();

            Element root = document.createElement( "dictionary" );

            //Create language section
            Element languageRoot = document.createElement( LANGUAGE_BEGIN );    //<languages>
            root.appendChild( languageRoot );
            for ( Language language : content.getFirst() )
            {
                Element languageElement = document.createElement( LANGUAGE_TAG_NAME );          //<language></language>
                languageElement.setAttribute( LANGUAGE_ATTR_NAME, language.getLanguageKey() );   //<language key="en"></language>
                languageElement.setTextContent( language.getLanguageName() );                   //<language key="en">English</language>
                languageRoot.appendChild( languageElement );
            }

            //Create translation section
            Element translationRoot = document.createElement( TRANSLATION_BEGIN );//<translations>
            root.appendChild( translationRoot );
            for ( IOriginalText originalText : content.getSecond() )
            {

                Element translationElement = document.createElement( TRANSLATION_TAG_NAME );       //<translation>
                translationElement.setAttribute( TRANSLATION_ATTR_NAME, originalText.getText() );   //<translation original="Test">
                translationRoot.appendChild( translationElement );

                for ( Translation translation : originalText.getTranslations() )
                {
                    Element textElement = document.createElement( TAG_TEXT_NAME );           //<text></text>
                    textElement.setAttribute( ATTR_LANG_NAME, translation.getLanguage() );   //<text lang="en"></text>
                    if ( !translation.isReplaceable() )
                        textElement.setAttribute( PROTECTED_TAG, "true" );               //<text lang="en" protected="true"></text>
                    textElement.setTextContent( translation.getTranslated() );               //<text lang="en">something</text>
                    translationElement.appendChild( textElement );                              //add <text> to <translation> node
                }

            }

            //Add to file
            document.appendChild( root );

            //Save file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource( document );
            StreamResult result = new StreamResult( getDataFileOutStream() );
            transformer.transform( source, result );

        }
        catch ( ParserConfigurationException | TransformerException e )
        {
            e.printStackTrace();
        }
    }


}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/