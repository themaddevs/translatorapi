package de.spaceparrots.translator.core;

import de.spaceparrots.translator.api.IOriginalText;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * <h1>OriginalText</h1>
 * The {@link IOriginalText} implementation.
 * The OriginalText containing references
 * to all its translations.
 *
 * @author Drayke
 * @version 1.0
 * @since 17.07.2017
 */
@Data
public final class OriginalText implements IOriginalText
{

    private String text;

    private List<Translation> translations;

    private Translation original;

    /**
     * Instantiates a new OriginalText.
     *
     * @param text the text
     */
    public OriginalText( String text )
    {
        this( text, new ArrayList<Translation>() );
    }

    /**
     * Instantiates a new OriginalText.
     *
     * @param text            the text
     * @param translationList the translation list
     */
    public OriginalText( String text, List<Translation> translationList )
    {
        this.text = text;
        this.translations = translationList;
        this.original = new Translation( "", text );
    }

    /**
     * Compares the OriginalText instance by their original text.
     *
     * @param text the text
     *
     * @return true if equal, else false
     */
    public boolean equals( String text )
    {
        return this.text.equals( text );
    }

    /**
     * Merges two OriginalText objects. The original translations are going
     * to be overwritten when no protected tag is present.
     *
     * @param other the OriginalText to merge with
     *
     * @return the amount of merged translations
     */
    @Override
    public int merge( IOriginalText other )
    {
        int merged = 0;
        if ( other.equals( this.text ) )
        {
            //Iterate this translations
            for ( Translation tr : getTranslations() )
            {
                //Change a translation
                String newTranslation = other.getTranslation( tr.getLanguage() ).getTranslated();
                //This method just works when it's replaceable
                if ( tr.setTranslation( newTranslation ) )
                    merged++;
            }
            //Iterate other translations
            for ( Translation tr : other.getTranslations() )
            {
                //Check if already there
                if ( !hasTranslation( tr.getLanguage() ) )
                {
                    addTranslation( tr.getLanguage(), tr.getTranslated(), tr.isReplaceable() );
                    merged++;
                }
            }
        }
        return merged;
    }

    /**
     * Adds a translation.
     * A translation will be added when there is no one present for
     * the given language, or when the present translation is replaceable
     *
     * @param languageKey the language key
     * @param translation the translation
     * @param replaceable the replaceable
     */
    public void addTranslation( String languageKey, String translation, boolean replaceable )
    {
        Translation tr = getTranslation( languageKey );
        if ( tr == null || tr.isReplaceable() )
        {
            translations.remove( tr );
            translations.add( new Translation( languageKey, translation, replaceable ) );
        }
    }

    /**
     * Gets a translation by the language key.
     *
     * @param languageKey the language key
     *
     * @return the translation
     */
    public Translation getTranslation( String languageKey )
    {
        Optional<Translation> translation = translations.stream().filter( trans -> trans.getLanguage().equals( languageKey ) ).findFirst();
        if ( !translation.isPresent() )
            return null;
        return translation.get();
    }

    /**
     * Checks if a translation is present for a given language.
     *
     * @param languageKey the language key
     *
     * @return the boolean
     */
    public boolean hasTranslation( String languageKey )
    {
        return getTranslation( languageKey ) != null;
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( !( obj instanceof OriginalText ) )
            return false;
        return ( (OriginalText) obj ).getText().equals( this.text );
    }

    @Override
    public int hashCode()
    {
        return this.text.hashCode();
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/