package de.spaceparrots.translator.core;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <h1>Language</h1>
 * A Language with a language key and name.
 *
 * @author Drayke
 * @version 1.0
 * @since 09.06.2017.
 */
@Getter
@AllArgsConstructor
public final class Language
{

    /**
     * The shortcode for the language. This
     * field is also used as an identifier for
     * the language.
     * Example: "de","en",..
     */
    private final String languageKey;

    /**
     * The name of the language in their language.
     * Example: "Deutsch","English"
     */
    private String languageName;


    @Override
    public String toString()
    {
        return this.languageName;
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( !( obj instanceof Language ) ) return false;
        return ( (Language) obj ).getLanguageKey().equals( this.languageKey );
    }

    @Override
    public int hashCode()
    {
        return this.languageKey.hashCode();
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/