package de.spaceparrots.translator.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <h1>Pair</h1>
 * A Container for two Types building a pair.
 *
 * @param <L> the "left" type parameter
 * @param <R> the "right" type parameter
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 12.02.2017
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Pair<L, R> implements Comparable<Pair<L, R>>
{

    private L first;
    private R second;

    /**
     * Swaps the two types of the pair.
     *
     * @return the new type swapped pair
     */
    public final Pair<R, L> swap()
    {
        return new Pair<R, L>( getSecond(), getFirst() );
    }

    public int compareTo( Pair<L, R> other )
    {
        if ( first instanceof Comparable )
        {
            Comparable<L> f = (Comparable<L>) first;
            int comp = f.compareTo( other.getFirst() );
            if ( comp != 0 ) return comp;
            if ( second instanceof Comparable )
            {
                Comparable<R> s = (Comparable<R>) second;
                comp = s.compareTo( other.getSecond() );
                return comp;
            }
            else comp = second.toString().compareTo( other.getSecond().toString() );
            return comp;
        }
        else return first.toString().compareTo( other.getFirst().toString() );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/