package de.spaceparrots.translator.util;

import de.spaceparrots.translator.util.exception.TranslatorFileException;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.*;

/**
 * <h1>AXmlStorageFile</h1>
 * The abstract class for the XMLFiles used to store and
 * read data by the dictionary and the editor.
 *
 * @author Drayke
 * @version 1.0
 * @since 14.06.2017
 */
public abstract class AXmlStorageFile
{

    /**
     * Returns an InputStream of the data file
     * @see InputStream
     * @return InputStream of the file
     */
    @Getter(AccessLevel.PROTECTED)
    private InputStream dataFileInStream = null;

    /**
     * Returns an OutputStream of the data file
     * @see OutputStream
     * @return OutputStream of the file
     */
    @Getter(AccessLevel.PROTECTED)
    private OutputStream dataFileOutStream = null;

    /**
     * Instantiates a new XmlTranslatorFile for
     * accessing the stored data.
     *
     * @param inputStream the input stream
     */
    public AXmlStorageFile( InputStream inputStream )
    {
        if ( inputStream == null ) throw new TranslatorFileException( TranslatorFileException.Cause.STREAM_NULL );
        dataFileInStream = inputStream;
    }

    /**
     * Instantiates a new XmlTranslatorFile for
     * parsing and store data into a file.
     *
     * @param outputStream the output stream
     */
    public AXmlStorageFile( OutputStream outputStream )
    {
        if ( outputStream == null ) throw new TranslatorFileException( TranslatorFileException.Cause.STREAM_NULL );
        dataFileOutStream = outputStream;
    }

    /**
     * Instantiates a new XmlTranslatorFile for
     * accessing the stored data.
     *
     * @param file the file
     */
    public AXmlStorageFile( File file )
    {
        this( file, Action.OPEN );
    }

    /**
     * Instantiates a new XmlTranslatorFile with a
     * given use case.
     *
     * @param file   the file
     * @param action the action
     */
    public AXmlStorageFile( File file, Action action )
    {

        switch ( action )
        {
            case OPEN:
                //Check if file is present
                if ( !file.exists() )
                {
                    throw new TranslatorFileException( TranslatorFileException.Cause.FILE_NOT_FOUND, file.getAbsolutePath() );
                }
                //Open stream
                try
                {
                    dataFileInStream = new FileInputStream( file );
                }
                catch ( FileNotFoundException e )
                {
                    throw new TranslatorFileException( TranslatorFileException.Cause.FILE_NOT_FOUND, file.getAbsolutePath() );
                }
                break;
            case CREATE:
                try
                {
                    file.createNewFile();
                    dataFileOutStream = new FileOutputStream( file );
                }
                catch ( FileNotFoundException e )
                {
                    throw new TranslatorFileException( TranslatorFileException.Cause.FILE_NOT_FOUND, file.getAbsolutePath() );
                }
                catch ( IOException e )
                {
                    throw new TranslatorFileException( TranslatorFileException.Cause.CREATE, file.getAbsolutePath() );
                }
                break;
        }


    }

    /**
     * Indicated if this XmlTranslatorFile usable.
     *
     * @return returns true if in/outstreams are good, else false
     */
    public boolean isUsable()
    {
        return dataFileOutStream != null || dataFileInStream != null;
    }

    /**
     * The Action enum for the XmlTranslatorFile use case
     * given by the constructor.
     */
    public enum Action
    {
        /**
         * Action: Open a file.
         */
        OPEN,
        /**
         * Action: Create a file.
         */
        CREATE
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/