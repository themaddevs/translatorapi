package de.spaceparrots.translator.util.exception;

import lombok.Getter;

/**
 * <h1>TranslatorFileParsingException</h1>
 * This RuntimeException will be thrown while
 * parsing data from or to a TranslatorFile.
 *
 * @author Drayke
 * @version 1.0
 * @since 12.06.2017
 */
public final class TranslatorFileParsingException extends RuntimeException
{

    @Getter
    private String context;

    public TranslatorFileParsingException( String context)
    {
        super("LanguageFileParserException: "+context);
        this.context = context;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/