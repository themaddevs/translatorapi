package de.spaceparrots.translator.util.exception;

/**
 * <h1>ArgumentConverterException</h1>
 * This RuntimeException will be thrown by the Translator while
 * converting argument objects.
 *
 * @author Drayke
 * @version 1.0
 * @since 19.07.2017
 */
public final class ArgumentConverterException extends RuntimeException
{

    public ArgumentConverterException( Class<?> type, String message )
    {
        super( "ArgumentConverterException: Class:" + type.getName() + ", " + message );
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/