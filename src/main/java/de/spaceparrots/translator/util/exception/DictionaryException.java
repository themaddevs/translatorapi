package de.spaceparrots.translator.util.exception;

import lombok.Getter;


/**
 * <h1>DictionaryException</h1>
 * This RuntimeException will be thrown by the Dictionary
 * on several events like: wrong usage, missing translations,
 * and more.
 *
 * @author Drayke
 * @version 1.0
 * @since 12.06.2017
 */
public final class DictionaryException extends RuntimeException
{

    @Getter
    private String message;

    public DictionaryException(String message)
    {
        super("DictionaryException: "+message);
        this.message = message;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/