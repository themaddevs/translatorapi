package de.spaceparrots.translator.util.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <h1>TranslatorFileException</h1>
 * This RuntimeException will be thrown while
 * opening or creating a TranslatorFile.
 *
 * @author Drayke
 * @version 1.0
 * @since 12.06.2017
 */
public final class TranslatorFileException extends RuntimeException
{

    public TranslatorFileException( Cause cause )
    {
        this( cause, "" );
    }

    public TranslatorFileException( Cause cause, String message )
    {
        super( cause.getMessage() + ( message.isEmpty() ? "" : ( " : " + message ) ) );
    }

    @Getter
    @AllArgsConstructor
    public enum Cause
    {
        STREAM_NULL( "The file stream is null!" ),
        WRONG_FILE( "A \".xml\" file is required! " ),
        FILE_NOT_FOUND( "Could not open or find the language file!" ),
        CREATE( "Could not create the file!" );
        private String message;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots (c) copyright 2018
 *
 ***********************************************************************************************/