# TranslatorAPI #

This TranslatorAPI shall enable other developers making their applications multilingual.
The goal is to make the process in generating language files as easy as possible.

This project uses a JavaFX Editor called MadLinguist for easy translation.

### Status: ###
* API: Working, but not 100% tested.
* Editor: Working, but not 100% tested.
* SpigotPlugin: Working, but not 100% tested.

## How does is work? ##

* Use the `Translator.tr("Demo")` method for translation
* Open the [MadLinguist - http://drayke.de/download/madlinguist/](www.drayke.de/download/madlinguist) Editor and scan your resource files. After manual translation a `dictionary.xml` will be generated. 
* Use the generated `dictionary.xml` to register it within the `Dictionary` class

> For more information visit the wiki.

## MadLinguist ##
* Source & Download: https://bitbucket.org/themaddevs/madlinguist

## Spigot Version ##
* If you want multilingual support on your spigot projects you need to have this plugin installed on your server. Don't forget to add a dependency inside the plugin.yml
* Link: [TranslatorBukkit](https://bitbucket.org/themaddevs/translatorplugin)

## Requirements ##
* Java 1.8
* Lombok (https://projectlombok.org/)

### Java Docs ###
http://javadoc.drayke.de/

### Maven repository ###

```
#!xml

<repositories>
    <repository>
        <id>MadDevs</id>
        <url>http://repo.maddevs.de/repository/maven-public/</url>
    </repository>
</repositories>


<dependencies>
    <dependency>
        <groupId>de.maddevs</groupId>
        <artifactId>translator-api</artifactId>
        <version>1.1</version>
    </dependency>
</dependencies>


```
